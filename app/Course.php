<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'course';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
     public function getCourse($courseId = null){
         if($courseId){
             $table_course = $this->where('course_id','=', $courseId)->get();
             
             //sprawdzanie czy znalazł jakis rekord
             if(!$table_course->count()){
                 return false;
             } else {
                return $table_course;
             } 
             
         } else {
             $table_course = $this->get()->all();
             return $table_course;
         }
     }

    public function getCourseByUser($userId)
    {
        $table_course = $this->join('user_course', 'course.course_id', '=', 'user_course.course_id')->
                                where('user_course.user_id', '=', $userId)->get()->all();
        //sprawdzanie czy znalazł jakis rekord
        if (empty($table_course)) {
            return array();
        } else {
            return $table_course;
        }
    }

}