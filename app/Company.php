<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User_class;

class Company extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     * 
     */
     
    protected $table = 'company';
    
    
    public function getCompany($company_id = null){
        if($company_id){
            
            $company_table = $this->where('company_id', $company_id)->get()->all();
            
            $output = array();
            
            foreach($company_table as $company){
              
                $single_company = array(
                                        'id'        => $company['company_id'],
                                        'name'      => $company['company_name'],
                                        'location'  => $company['location']
                    );
                    
                $output[]  = $single_company;
            }
            
            return $output;
            
        } else {
            
            $company_table = $this->get()->all();
            
            $output = array();
            
            foreach($company_table as $company){
                
                $single_company = array(
                                        'id'        => $company['company_id'],
                                        'name'      => $company['company_name'],
                                        'location'  => $company['location']
                    );
                    
                $output[]  = $single_company;
            }
            
            return $output;
        }
        
    }

}