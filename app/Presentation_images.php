<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentation_images extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'presentation_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
     public function getImageByPresentationId($presentation_id){
            $table_images = $this->join('image','presentation_images.image_id','=','image.image_id')->
                                where('presentation_images.presentatnion_id', $presentation_id)->get();
            if(!$table_images->count()){
                return array();
             } else {
                return $table_images;
             } 
    }
     
}