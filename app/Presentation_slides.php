<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentation_slides extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'presentation_slides';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];
    
    public function getSlidesList($presentation_id){
        $slides_table = $this->join('image', 'image.image_id', '=', 'presentation_slides.image_id')->where('presentatnion_id', $presentation_id)->orderBy('slides_id', 'asc')->get()->all();
        $output =  array();
        foreach($slides_table as $slide){
            $output[] = array(
                                'presentation_id' => $slide['presentation_id'],
                                'path' => $slide['path']
                             );
        }
        
        return $output;
    } 
}