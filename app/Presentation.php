<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\presentation_slides;


class Presentation extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'presentation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
     
     public function getPresentationByCourseId($course_id){
            $table_presentation = $this->where('course_id', $course_id)->get();
            if(!$table_presentation->count()){
                 return array();
             } else {
                return $table_presentation;
             } 
    }
    
    
    public function getPresentation($presentation_id){
            $output = array();
            $slides = new presentation_slides;
            if($presentation_id){
                
                $table_presentation = $this->join('image', 'presentation.image_id', '=', 'image.image_id')->where('presentation.presentatnion_id', $presentation_id)->get()->first();
                    $output = array(
                                    'presentation_id' => $table_presentation->original['presentatnion_id'],
                                    'title' => $table_presentation->original['title'],
                                    'description' => $table_presentation->original['description'],
                                    'image' => $table_presentation->original['path'],
                                    'answerable' => $table_presentation->original['answerable'],
                                    'course_id' => $table_presentation->original['course_id'],
                                    'slides' => $slides->getSlidesList($table_presentation->original['presentatnion_id'])
                                    );
                echo json_encode($output);
                
                
            } else {
               
                $table_presentation = $this->join('image', 'presentation.image_id', '=', 'image.image_id')->get()->all();
                foreach($table_presentation as $presentation) {   
                    $output[] = array(
                                    'presentation_id' => $presentation['presentatnion_id'],
                                    'title' => $presentation['title'],
                                    'description' => $presentation['description'],
                                    'image' => $presentation['path'],
                                    'answerable' => $presentation['answerable'],
                                    'course_id' => $presentation['course_id'],
                                    'slides' => $slides->getSlidesList($presentation['presentatnion_id'])
                                    );
                }
                echo json_encode($output);
            }
            
            
    }
     
}