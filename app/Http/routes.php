<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::group(['prefix' => 'api/v1', 'middleware' => 'jwt.auth'], function(){
Route::get('/', function () {
    echo bcrypt('haslo');
    die;
    return view('welcome');
});
Route::post('authenticate', 'Auth\AuthController@authenticate');
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('image/{image}','ApiImagesController@get');
    Route::get('video/{filename}', 'ApiVideoController@download')->where('filename', '^[^/]+$');
});

Route::group(['prefix' => 'api/v1', 'middleware' => 'jwt.auth'], function(){
    Route::post('video/upload', 'ApiVideoController@upload');
    Route::get('me', 'Auth\AuthController@show');
    Route::get('news/{newsId?}', 'ApiNewsController@get');
    Route::get('course/{courseId?}', 'ApiCourseController@get');
    Route::get('class/{classId?}', 'ApiClassController@get');
    Route::get('company/{companyId?}', 'ApiCompanyController@get');
    Route::get('{userId}/userAnswer', 'ApiUserAnswerController@getUserAnswer');
    Route::get('{userId}/course', 'ApiCourseController@getCourse');
    Route::get('{userId}/class', 'ApiClassController@getClass');
    Route::get('{userId}/news', 'ApiNewsController@getNews');
});

//Route::group(['middleware' => ['ApiAuthentication']], (function () {
   // Route::get('api/news/{newsId?}', 'ApiGetController@getNews');
  //  Route::get('api/course/{courseId?}', 'ApiGetController@getCourse');
//    Route::get('api/video/{videoId?}', 'ApiGetController@getVideo');
//    Route::get('api/class/{classId?}', 'ApiGetController@getClass');
//    Route::get('api/userAnswer/{answerId}', 'ApiGetController@getUserAnswer');
//    Route::get('api/company/{companyId?}', 'ApiGetController@getCompany');
//    Route::get('api/user/{userId?}', 'ApiGetController@getUser');
//    Route::get('api/me/', 'ApiGetController@getMe');
//    Route::get('api/class/{classId?}', 'ApiGetController@getClass');
//    Route::get('api/{userId}/news/', 'ApiGetController@getUserNews');
//    Route::get('api/{userId}/course/', 'ApiGetController@getUserCourse');
//    Route::get('api/{userId}/userAnswer/', 'ApiGetController@getUserUserAnswer');
//    Route::get('api/{userId}/class/', 'ApiGetController@getUserClass');
//    Route::get('api/presentations/{presentationId?}', 'ApiGetController@getPresentation');
    //Route::post('api/video/upload', 'ApiPostController@videoUpload');
    //Route::post('api/video/download', 'ApiPostController@videoDownload');
//}));

//Route::post('api/token/', 'ApiPostController@postToken');
//Route::group(['middleware' => 'jwt.auth'], function(){
//    Route::get('users', 'Auth\AuthController@index');
//    Route::get('user', 'Auth\AuthController@show');
//});
//Route::get('users', 'Auth\AuthController@index');
//Route::get('user/{id}', 'Auth\AuthController@show');
