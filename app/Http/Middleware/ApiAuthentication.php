<?php

namespace App\Http\Middleware;
use \Illuminate\Http\Request;
use \Closure;
use App\Token;
class ApiAuthentication
{
    protected $auth;
    // /**
    //  * Handle an incoming request.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  \Closure  $next
    //
    //  * @return mixed
    //  */
     public function handle(Request $request, Closure $next)
     {
         $token = new Token;
         $token->cleanTokens();
         if($request->header('SIMPLE-API-KEY')) {
             if ($token->authToken($request->header('SIMPLE-API-KEY'))) {
                if($token->authToken($request->header('SIMPLE-API-KEY') == 2)){
                     return response('Unauthorized request, token expired');
                } else {
                    return $next($request);
                }
             } else {
                 return response('Unauthorized request, incorrect SIMPLE-API-KEY ', 401);
             }
         } else {
             return response('SIMPLE-API-KEY required', 401);
         }
     }
}