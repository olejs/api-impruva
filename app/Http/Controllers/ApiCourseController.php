<?php
/**
 * Created by PhpStorm.
 * User: olejs
 * Date: 14.12.17
 * Time: 18:01
 */

namespace App\Http\Controllers;

use App\Course;
use App\Image;
use App\Presentation;
use App\Presentation_images;
use App\Video_lesson;
use Illuminate\Http\Request;
use Validator;

class ApiCourseController extends Controller
{
    /**
     * @param Course $course
     * @param Request $request
     * @param null $courseId
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Course $course, Request $request, $courseId = null){

        $arrayToValidation = array_merge($request->all(),
            ['courseId' => $request->route('courseId')]);

        $validation = Validator::make($arrayToValidation, [
            'courseId' => 'integer|exists:course,course_id'
        ]);

        if($validation->fails()){
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }

        $output = $course->getCourse($courseId);
        $json_array                 = array();
        $presentation_array         = array();
        $presentation_images_array  = array();
        $video_array                = array();

        $presentation = new presentation;
        $presentation_images = new presentation_images;
        $video = new video_lesson;
        $image = new image;

        foreach ($output as $element) {

            foreach($video->getVideoByCourseId($element['course_id']) as $movie){

                $video_array[] = array('video_id'         => $movie['video_id'],
                    'title'      => $movie['title'],
                    'image_id'   => $movie['image_id'],
                    'stream_url' => $movie['path'],
                    'duration'   => $movie['duration'],
                    'answerable' => $movie['answerable']);
            }

            foreach($presentation->getPresentationByCourseId($element['course_id']) as $presentation){
                foreach($presentation_images->getImageByPresentationId($presentation['presentatnion_id']) as $presentation_image){
                    $presentation_images_array[] = array('image_id'  => $presentation_image['image_id'],
                        'url' => $presentation_image['path'],
                        'slide_id' => $presentation_image['slide_id']);
                }
                $presentation_array[] = array('presentation_id'          => $presentation['presentatnion_id'],
                    'description' => $presentation['description'],
                    'title'       => $presentation['title'],
                    'slides'      => $presentation_images_array,
                    'answerable'  => $presentation['answerable']);

            }
            $image_url = $image->getImage($element['image_id']);

            $json_array[] = array('course_id'           => $element['course_id'],
                'title'         => $element['title'],
                'image'         => $image_url['path'],
                'videos'        => $video_array,
                'presentations' => $presentation_array);
        }
        return response()->json($json_array);
    }

    public function getCourse(Course $course, Request $request, $userId){

        $arrayToValidation = array_merge($request->all(),
            ['userId' => $request->route('userId')]);

        $validation = Validator::make($arrayToValidation, [
            'userId' => 'integer|exists:user,user_id'
        ]);

        if ($validation->fails()) {
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }

        $output = $course->getCourseByUser($userId);
        $json_array                 = array();
        $presentation_array         = array();
        $presentation_images_array  = array();
        $video_array                = array();

        $presentation = new presentation;
        $presentation_images = new presentation_images;
        $video = new video_lesson;
        $image = new image;

        foreach ($output as $element) {

            foreach($video->getVideoByCourseId($element['course_id']) as $movie){

                $video_array[] = array('video_id'         => $movie['video_id'],
                    'title'      => $movie['title'],
                    'image_id'   => $movie['image_id'],
                    'stream_url' => $movie['path'],
                    'duration'   => $movie['duration'],
                    'answerable' => $movie['answerable']);
            }

            foreach($presentation->getPresentationByCourseId($element['course_id']) as $presentation){
                foreach($presentation_images->getImageByPresentationId($presentation['presentatnion_id']) as $presentation_image){
                    $presentation_images_array[] = array('image_id'  => $presentation_image['image_id'],
                        'url' => $presentation_image['path'],
                        'slide_id' => $presentation_image['slide_id']);
                }
                $presentation_array[] = array('presentation_id'          => $presentation['presentatnion_id'],
                    'description' => $presentation['description'],
                    'title'       => $presentation['title'],
                    'slides'      => $presentation_images_array,
                    'answerable'  => $presentation['answerable']);

            }
            $image_url = $image->getImage($element['image_id']);

            $json_array[] = array('course_id'           => $element['course_id'],
                'title'         => $element['title'],
                'image'         => $image_url['path'],
                'videos'        => $video_array,
                'presentations' => $presentation_array);
        }
        if(count($json_array) == 1) {
            return response()->json($json_array[0]);
        } else {
            return response()->json($json_array);
        }
    }
}