<?php
/**
 * Created by PhpStorm.
 * User: olejs
 * Date: 14.12.17
 * Time: 17:30
 */

namespace App\Http\Controllers;


use App\News;
use Illuminate\Http\Request;
use Validator;

class ApiNewsController extends Controller
{
    public function get(News $news, Request $request, $newsId = null){

        $arrayToValidation = array_merge($request->all(),
            ['newsId' => $request->route('newsId')]);

        $validation = Validator::make($arrayToValidation, [
            'newsId' => 'integer|exists:News,news_id'
        ]);

        if($validation->fails()){
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }

        $output = $news->getNews($newsId);

        $json_array = array();
        foreach ($output as $element) {
            $json_array[] = array(
                'news_id' => $element['news_id'],
                'text' => $element['text'],
                'type' => $element['type'],
                'userId' => $element['user_id']);
        }
        return response()->json(array_reverse($json_array));
    }

    public function getNews(News $news, Request $request, $userId){
        $arrayToValidation = array_merge($request->all(),
            ['userId' => $request->route('userId')]);

        $validation = Validator::make($arrayToValidation, [
            'userId' => 'integer|exists:user,user_id'
        ]);

        if ($validation->fails()) {
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }

        $array = $news->where('user_id', $userId)->get()->all();

        $output = array();

        foreach($array as $element){
            $output[] = array(
                'news_id' => $element['news_id'],
                'text' => $element['text'],
                'type' => $element['type'],
                'userId' => $element['user_id']
            );
        }

        return response()->json($output);
    }
}