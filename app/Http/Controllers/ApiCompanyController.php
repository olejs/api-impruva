<?php
/**
 * Created by PhpStorm.
 * User: olejs
 * Date: 16.12.17
 * Time: 16:55
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Validator;

class ApiCompanyController extends Controller
{
    public function get(Company $company, Request $request, $companyId = null)
    {
        $arrayToValidation = array_merge($request->all(),
            ['companyId ' => $request->route('companyId ')]);

        $validation = Validator::make($arrayToValidation, [
            'companyId' => 'integer|exists:company,company_id '
        ]);

        if ($validation->fails()) {
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }
        $output = $company->getCompany($companyId);
        return response()->json($output);
    }
}