<?php
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Role;
use App\api_keys;
use App\Token;
use App\Video;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Validator;
use Illuminate\Support\Facades\Storage;

class ApiPostController extends Controller
{
     public function authRequestData($current_key){
          $token = new Token;
          return $token->authToken($current_key);
     }
     
     public function postToken(Request $request, User $user){
         $token = new Token;
         $user = new User;
         $token->cleanTokens();
         $login = $request->input('user');

         if(!($login)){
             return response("Unauthorized request, 'user' field required", 400);
         }

         $password = $request->input('password');

         if(!($password)){
             return response("Unauthorized request, 'password' field required", 400);
         }

         $output = $user->getUser($login, $password);
         if($output){
             $tokenInfo = $token->getTokenInformation($output['user_id']);
             if($tokenInfo['expires_in'] > date('Y-m-d G:i:s'))
             {
                 $newExpiration = $token->updateToken($output['access_token']);
                 $json = array('Token' => array(
                     'access_token' => $tokenInfo['access_token'],
                     'expires_in'   => $newExpiration,
                     'issued'       => $tokenInfo['issued'],
                     'user_id'      => $tokenInfo['user_id']
                     ));
                 return  response()->json($json);

             } else {
                 $json = $user->insertNewUserToken($login, $password);
                 return  response()->json($json);
             }

         } else {

             return response('Unauthorized request, incorrect login or password');

         }
    }


    
    public function videoUpload(Request $request, Token $token){

            if($request->hasFile('video') && !empty($request->input('title')) && !empty($request->input('description')) && !empty($request->input('answerable')) && !empty($request->input('course_id')) && !empty($request->input('duration'))){ 
                
                $title = $request->input('title');
                $description = $request->input('description');
                $answerable = $request->input('answerable');
                $course_id = $request->input('course_id');
                $duration = $request->input('duration');
                $video = $request->file('video');
                $videoName = $video->getClientOriginalName();
                $userId = $token->getUserByToken($request->header('SIMPLE-API-KEY'));
                $videoModel = new Video;
                $videoModel->saveVideo($videoName, $duration, $userId, $title, $description, $answerable, $course_id);
                $video->move('videos', $videoName);
                
            } else {
                
                $json = array('Error' => 
                            array(
                                  'status'  => '400',
                                  'message' => 'INVALID REQUEST DATA'
                ));
                
                echo json_encode($json);
                abort(400);
            }
    }
    
    public function videoDownload(Request $request, Video $video){
        if($this->authRequestData($request->header('SIMPLE-API-KEY'))){
            if(!empty($request->input('video_id'))){
                
                $video_id = $request->input('video_id');
                $output = $video->where('video_id', $video_id)->get()->first();
                response()->download($output->path);
            
            } else {
                
                $json = array('Error' => 
                            array(
                                  'status'  => '400',
                                  'message' => 'INVALID REQUEST DATA'
                ));
                
                echo json_encode($json);
                abort(400);
            }
            
        } else {
            
            abort(401, 'Unauthorized request'); 
            
        }
        
    }
}