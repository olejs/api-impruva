<?php
/**
 * Created by PhpStorm.
 * User: olejs
 * Date: 15.12.17
 * Time: 19:15
 */

namespace App\Http\Controllers;

use App\Classes;
use Illuminate\Http\Request;
use Validator;

class ApiClassController extends Controller
{
    public function get(Classes $class, $classId = null){
        return response()->json($class->getClass($classId));
    }

    public function getClass(Classes $class, Request $request, $userId){
        $arrayToValidation = array_merge($request->all(),
            ['userId' => $request->route('userId')]);

        $validation = Validator::make($arrayToValidation, [
            'userId' => 'integer|exists:user,user_id'
        ]);

        if ($validation->fails()) {
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }

        $array = $class->join('user_class', 'user_class.class_id', '=', 'class.class_id')
                       ->where('user_class.user_id', $userId)->get()->all();

        $output = array();
        foreach($array as $element){
            $output[] = array(
                'classId' => $element['class_id'],
                'className' => $element['class_name'],
                'companyId' => $element['company_id']
            );
        }

        return response()->json($output);
    }
}