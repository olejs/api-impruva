<?php
namespace App\Http\Controllers;

use App\Classes;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Course;
use App\api_keys;
use App\Video;
use App\Token;
use App\News;
use App\presentation;
use App\presentation_images;
use App\image;
use App\video_lesson;
use App\company;

class ApiGetController extends Controller
{
   public function getNews(News $news, Request $request, $newsId = null){
            $output = $news->getNews($newsId);
            if(!$output){
                return response('Bad news Id', 200);
            } else {
                $json_array = array();
                 foreach ($output as $element) {
                   $json_array[] = array('text' => $element['text'], 
                                                           'type' => $element['type'],
                                                           'userId' => $element['user_id']);
                }
                echo json_encode(array_reverse($json_array));
            }
   }
   
   public function getCourse(Course $course, Request $request, $courseId = null){
            $output = $course->getCourse($courseId);
            if(!$output){
                return response('Bad course Id', 200);
            } else {
                $json_array = array();
                $presentation_array = array();
                $presentation_images_array = array();
                $video_array = array();
                $presentation = new presentation;
                $presentation_images = new presentation_images;
                $video = new video_lesson;
                $image = new image;
                foreach ($output as $element) {
                    //get presentations by course id
                    foreach($video->getVideoByCourseId($element['course_id']) as $movie){
                        
                        $video_array[] = array('id'         => $movie['video_id'],
                                               'title'      => $movie['title'],
                                               'image_id'   => $movie['image_id'],
                                               'stream_url' => $movie['path'],
                                               'duration'   => $movie['duration']);
                    }
                    
                    foreach($presentation->getPresentationByCourseId($element['course_id']) as $presentation){
                        foreach($presentation_images->getImageByPresentationId($presentation['presentatnion_id']) as $presentation_image){
                            $presentation_images_array[] = array('id'  => $presentation_image['image_id'],
                                                                 'url' => $presentation_image['path']);
                        }
                        $presentation_array[] = array('id'          => $presentation['presentatnion_id'],
                                                      'description' => $presentation['description'],
                                                      'title'       => $presentation['title'],
                                                      'slides'      => $presentation_images_array,
                                                      'answerable'  => $presentation['answerable']);
                                                     
                    }
                    $image_url = $image->getImage($element['image_id']);

                    $json_array[] = array('id'           => $element['course_id'],
                                         'title'         => $element['title'],
                                         'image'         => $image_url['path'],
                                         'videos'        => $video_array,
                                         'presentations' => $presentation_array);
                }
                return response()->json($json_array);
            }
   }
   
   public function getVideo(Video $video, Request $request, $videoId = null){
            $output = $video->getVideo($videoId);
            if(!$output){
                return response('Bad video Id', 200);
            } else {
                $json_array = array();
                foreach ($output as $element) {
                   $json_array =   array('id'         => $element['video_id'],
                                         'title'      => $element['title'],
                                         'image_id'   => $element['image_id'],
                                         'stream_url' => $element['path'],
                                         'duration'   => $element['duration'],
                                         'answerable' => $element['aswerable']);
                }
                echo json_encode($json_array);
            }
   }
   
   public function getClass(Classes $class, Request $request, $classId = null){
       $class->getClass($classId);
   }
   
   public function getUserAnswer(Video $video, $answerId){
       $video->getVideoById($answerId);
   }
   
   public function getCompany(company $company, Request $request, $companyId = null){
       $company->getCompany($companyId);
   }
   
   public function getUser(User $user, Request $request, $userId = null){
       $user->getUsers($userId);
   }

   public function getUserNews(User $user, Request $request, $userId){
       $user->getNewsByUserId($userId);
   }
   
   public function getUserCourse(User $user, Request $request, $userId){
       $user->getCourseByUserId($userId);
   }
   
   public function getUserUserAnswer(User $user, Request $request, $userId){
       $user->getUserAnswerByUserId($userId);
   }
   
   public function getUserClass(User $user, Request $request, $userId = null){

   }
   
   public function getPresentation(presentation $presentation, Request $request, $presentation_id = null){
       $presentation->getPresentation($presentation_id);
   }
   
   public function videoUpload(Request $request){

   }
   
}


