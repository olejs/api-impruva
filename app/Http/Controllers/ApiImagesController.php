<?php
/**
 * Created by PhpStorm.
 * User: olejs
 * Date: 17.12.17
 * Time: 10:53
 */

namespace App\Http\Controllers;


use App\Image;
use Illuminate\Http\Request;
use Validator;

class ApiImagesController extends Controller
{
    public function get(Request $request, $image = null){
        $arrayToValidation = array_merge($request->all(),
            ['image' => $request->route('image')]);

        $validation = Validator::make($arrayToValidation, [
            'image' => 'exists:image,path'
        ]);

        if($validation->fails()){
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }
        $path = storage_path().'/app/images/'.$image;

        if (file_exists($path)) {
            return response()->stream(function ($path) {

                readfile($path);

            }, 200, ['content-type' => 'image/jpeg']);
        } else {
            return response()->json(array('message' => 'file does not exist', 'status' => 404), 404);
        }
    }
}