<?php

namespace App\Http\Controllers\Auth;

use Hash;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request){
        $credentials = $request->only('email', 'password');
        try {
            if(!$token = JWTAuth::attempt($credentials)){
                return response()->json(['error' =>'User credentials are not correct'], 401);
            }
        } catch (JWTException $ex) {
            return response()->json(['error' => 'Something went wrong'], 500);
        }
        return response()->json(compact('token'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(){
        return User::all();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(){
        try {
            $user = JWTAuth::parseToken()->toUser();
            if(!$user){
                return $this->response->errorNotFound("User not found");
            }
        } catch (TokenInvalidException $ex) {
            return $this->response()->error('Token is invalid');
        } catch (TokenExpiredException $ex) {
            return $this->response()->error('Token has expired');
        } catch (TokenBlacklistedException $ex) {
            return $this->response()->error('Token is blacklisted');
        }
        $singleUser = new User();
        return response()->json($singleUser->getUsers($user['user_id']),200);
    }
}
