<?php
/**
 * Created by PhpStorm.
 * User: olejs
 * Date: 14.12.17
 * Time: 00:16
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Video;
use Validator;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Storage;

class ApiVideoController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request){

        $validation = Validator::make($request->all(), [
            'title' => 'required|string|max:100',
            'lessonId' => 'required_without:presentationId|exists:video_lesson,lesson_id',
            'presentationId' => 'required_without:lessonId|exists:presentation,presentation_id',
            'duration' => 'required|integer',
            'video' => 'required|mimes:mp4,ox,oga,ogv,ogg,webm|max:100000000',
            'events'=> 'string',
            'imageId' => 'integer|exists:image,image_id'
        ]);

        if($validation->fails()){
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }

        $user = JWTAuth::parseToken()->toUser();
        $file = $request->file('video');
        $mime = $file->getClientOriginalExtension();
        $name = $file->getClientOriginalName();
        $path = md5(time().$name);
        $video = new Video();

        $size = $file->getClientSize();

        $fullpath = 'videos/'.$path.'.'.$mime;

        $dataToSave = array(
            'title'      => $request->title,
            'answerable' => $request->answerable,
            'lesson_id'  => $request->lessonId,
            'duration'   => $request->duration,
            'path'       => $fullpath,
            'user_id'    => $user['user_id'],
            'events'     => isset($request->events) ? $request->events : null,
            'image_id'   => isset($request->imageId) ? $request->imageId : null,
            'presentation_id'=> isset($request->presenationId) ? $request->presenationId : null
        );

        $upload = Storage::disk('local')->put($fullpath, File::get($file));

        if($upload){
            $video->_save($dataToSave);
        } else {
            return response()->json(array('message' => 'file was not uploaded', 'status' => 400), 400);
        }
    }

    /**
     * @param $foldername
     * @param $filename
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($filename){
        $fullpath = "app/videos/{$filename}";

        if(!file_exists($fullpath)){
            return response()->json(array('message' => 'file does not exist', 'status' => 404), 404);
        } else {
            return response()->download(storage_path($fullpath), null, [], null);
        }
    }

    public function get(Video $video, Request $request, $videoId = null){

        $arrayToValidation = array_merge($request->all(),
            ['videoId' => $request->route('videoId')]);

        $validation = Validator::make($arrayToValidation, [
            'videoId' => 'integer|exists:video,video_id'
        ]);

        if($validation->fails()){
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }

        $output = $video->getVideo($videoId);

        $json_array = array();
        foreach ($output as $element) {
            $json_array[] = array('id' => $element['video_id'],
                'title' => $element['title'],
                'image_id' => $element['image_id'],
                'stream_url' => $element['path'],
                'duration' => $element['duration'],
                'answerable' => $element['aswerable']);
        }
        return response()->json($json_array);
    }
}