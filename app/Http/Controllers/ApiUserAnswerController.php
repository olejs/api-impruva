<?php
/**
 * Created by PhpStorm.
 * User: olejs
 * Date: 15.12.17
 * Time: 19:25
 */

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Validator;
use App\User_answer;

class ApiUserAnswerController extends Controller
{
    public function get(Video $video, Request $request, $answerId = null)
    {
        $arrayToValidation = array_merge($request->all(),
            ['answerId' => $request->route('answerId')]);

        $validation = Validator::make($arrayToValidation, [
            'answerId' => 'integer|exists:News,news_id'
        ]);

        if ($validation->fails()) {
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }

        return response()->json($video->getVideoById($answerId));
    }

    public function getUserAnswer(User_answer $user_answer, Request $request, $userId){

        $arrayToValidation = array_merge($request->all(),
            ['userId' => $request->route('userId')]);

        $validation = Validator::make($arrayToValidation, [
            'userId' => 'integer|exists:user,user_id'
        ]);

        if ($validation->fails()) {
            $errors = $validation->messages();
            return response()->json(array('message' => $errors, 'status' => 400), 400);
        }

        $output = $user_answer->join('video', 'video.video_id', '=', 'user_answer.video_id')->
                      where('user_id', '=', $userId)->get()->all();


        $outputToJson = array();

        foreach($output as $row){
            $outputToJson[] = array(
                'user_id' => $row['user_id'],
                'events' => $row['events'],
                'image_id' => $row['image_id'],
                'lesson_id' => $row['lesson_id'],
                'presentation_id' => $row['presentation_id'],
                'video_id' => $row['video_id'],
                'duration' => $row['duration'],
                'path'  => $row['path']
            );
        }

        return response()->json($outputToJson, 200);
    }
}