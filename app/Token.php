<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\User;

class Token extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'token';

    public $timestamps = false;
     public function authToken($api_key){//wazne
        $date = date('Y-m-d G:i:s');
        $result = $this->where('access_token', $api_key)->first();
        if(!empty($result)){
            if($date < $result['expires_in']) {
                return $result;
            } else {
                return 2;
            }
        } else {
            return false;
        }
    }
    
    public function generateToken($login, $user_id){
        $token = md5($login);
        $expires = date('Y-m-d G:i:s', strtotime('+1 hour'));
        $this->insert(['user_id' => $user_id, 'access_token' => $token, 'expires_in' => $expires, 'issued' => date('Y-m-d G:i:s')]);
        return $token;
    }
    
    public function tokenExist($token){
        $state = $this->where('access_token', $token)->get()->first();
        if($state){
            return true;
        } else {
            return false;
        }
    }
    
    public function getUserByToken($token){
        $user = $this->where('access_token', $token)->first();   
        $date = date('Y-m-d G:i:s');
        if(!empty($user) && $date < $user['expires_in']){
            return $user['user_id'];
        } else {
            return false;
        }
    }

    public function getTokenInformation($userId){
        return $this->where('user_id', $userId)->first();
    }

    public function cleanTokens(){//wazne
        $this->where('expires_in', '<', date('Y-m-d G:i:s'))->delete();
    }

    public function updateToken($token){
        $date = date('Y-m-d G:i:s', strtotime('+1 hour'));
        $this->where('access_token', $token)->update(['expires_in' => $date]);
        return $date;
    }
}