<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'image';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
     public function getImage($imageId){
         $image_array = $this->where('image_id', $imageId)->get()->first();
         if(!$image_array->count()){
            return null;
         } else {
            return $image_array; 
         }
     }
     
}