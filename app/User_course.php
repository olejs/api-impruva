<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_course extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_course';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
}