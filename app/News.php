<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'News';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
     public function getNews($news_id = null){
            if($news_id){
                $table_news = $this->where('news_id', $news_id)->get();
                if(!$table_news->count()){
                     return array();
                 } else {
                    return $table_news;
                 } 
            } else {
                $table_news = $this->get()->all();
                return $table_news;
            }
    }
     
}