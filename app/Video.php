<?php

namespace App;
use App\video_lesson;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Video extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'video';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
     public function getVideo($video_id = null){
         if($video_id){
             $video_table = $this->join('video_lesson','video.video_id','=','video_lesson.video_id')->
                                    where('video_lesson.video_id', $video_id)->get()->all();
             if(empty($video_table)){
                 return array();
             } else {
                 return $video_table;
             }
         } else {
            $video_table = $this->join('video_lesson','video.video_id','=','video_lesson.video_id')->get()->all();
            return $video_table;
         }
     }
     
     public function getVideoByCourseId($course_id){
         $video_table = $this->join('video_lesson','video.video_id','=','video_lesson.video_id')->
                                where('video_lesson.course_id', $course_id)->get()->all();

         if(empty($video_table)){
             return array();
         } else {
             return $video_table;
         }
     }
     
     public function getVideoByClassId($course_id){
         $video_table = $this->join('video_lesson','video.video_id','=','video_lesson.video_id')->
                                where('video_lesson.class_id', $course_id)->get()->all();

         if(empty($video_table)){
             return array();
         } else {
             return $video_table;
         }
     }
     
      public function getVideoById($video_id){
         $video_table = $this->join('video_lesson','video.video_id','=','video_lesson.video_id')->
                                where('video_lesson.video_id', $video_id)->get()->all();
         if(empty($video_table)){
             return array();
         } else {
             return $video_table;
         }
     }

    /**
     * @param $dataToSave
     */
    public function _save($dataToSave){

        $videoId = $this->insertGetId(
            [
                'path'      => $dataToSave['path'],
                'duration'  => $dataToSave['duration']
            ]

        );

        $userAnswer = new User_answer();

        $userAnswer->insert([
            'user_id' => $dataToSave['user_id'],
            'video_id'=> $videoId,
            'events'  => $dataToSave['events'],
            'image_id'=> $dataToSave['image_id'],
            'lesson_id'=>$dataToSave['lesson_id'],
            'presentation_id' => $dataToSave['presentation_id']
        ]);
     }

}