<?php

namespace App;

use Illuminate\Auth\Authenticatable as Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Video;
use App\user_course;
use App\Token;

class User extends Model implements Authenticatable
{

    use Auth;
    protected $table = 'user';

    protected $primaryKey ='user_id';

    public function getUser($login, $password){
        $table = $this->where('user_name', $login)->
                        where('password', $password)->get()->first();
        if(!empty($table)){
            return $table;
        } else {
            return False;
        }
    }

    public function insertNewUserToken($login, $password){
        $token = new Token;
        $date = date('Y-m-d G:i:s');
        $user = $this->getUser($login, $password);
        $access_token = md5($login . $date);
        $expires_in = date('Y-m-d G:i:s', strtotime('1 hour'));
        $token->insert(['user_id' => $user['user_id'], 'access_token' => $access_token, 'expires_in' => $expires_in, 'issued' => $date]);

        return array('user_id' => $user['user_id'],
                     'access_token' => $access_token,
                     'expires_in' => $expires_in,
                     'issued' => $date);
    }
    
    
    public function getUsers($user_id){
        
        if($user_id){
            $user_table = $this->join('user_class', 'user.user_id', '=', 'user_class.user_id')->
                                 join('class', 'user_class.class_id', '=', 'class.class_id')->
                                 join('image', 'user.avatar_id', '=', 'image.image_id')->
                                 join('company', 'class.company_id', '=', 'company.company_id')->
                                 join('role', 'user.role_id', '=', 'role.role_id')->where('user.user_id', $user_id)->get()->all();
            $output = array();
            foreach($user_table as $user){
                    $single_user = array(
                                         'user_id' => $user['user_id'],
                                         'email' => $user['email'],
                                         'firstName' => $user['first_name'],
                                         'lastName' => $user['last_name'],
                                         'is_stuff' => $user['is_stuff'],
                                         'avatar' => array('id' => $user['avatar_id'], 'url' => $user['path']),
                                         'role' => $user['role_name'],
                                         'company' => array('id' => $user['company_id'], 'name' => $user['company_name'], 'location' => $user['location'])
                        );
            
                $output = $single_user;
            }
            
            return $output;
            
        } else {
            
            $user_table = $this->join('user_class', 'user.user_id', '=', 'user_class.user_id')->
                                 join('class', 'user_class.class_id', '=', 'class.class_id')->
                                 join('image', 'user.avatar_id', '=', 'image.image_id')->
                                 join('company', 'class.company_id', '=', 'company.company_id')->
                                 join('role', 'user.role_id', '=', 'role.role_id')->get()->all();
            
            $output = array();
            foreach($user_table as $user){
             
                    $single_user = array(
                                         'user_id' => $user['user_id'],
                                         'user_name' => $user['user_name'],
                                         'firstName' => $user['first_name'],
                                         'lastName' => $user['last_name'],
                                         'is_stuff' => $user['is_stuff'],
                                         'avatar' => $user['path'],
                                         'role' => $user['role_name'],
                                         'company' => array('id' => $user['company_id'], 'name' => $user['company_name'], 'location' => $user['location'])
                        );
            
                $output[] = $single_user;
            }
            
            return $output;
        }
        
    }
    
    public function getUsersByApiKey($ApiKey){
        
        $user_table = $this->join('user_class', 'user.user_id', '=', 'user_class.user_id')->
                                 join('class', 'user_class.class_id', '=', 'class.class_id')->
                                 join('image', 'user.avatar_id', '=', 'image.image_id')->
                                 join('company', 'class.company_id', '=', 'company.company_id')->
                                 join('token', 'user.user_id', '=', 'token.user_id')->
                                 join('role', 'user.role_id', '=', 'role.role_id')->where('token.access_token', $ApiKey)->get()->all();
            $output = array();
            foreach($user_table as $user){
                    $single_user = array(
                                         'id' => $user['user_id'],
                                         'user_name' => $user['user_name'],
                                         'firstName' => $user['first_name'],
                                         'lastName' => $user['last_name'],
                                         'is_stuff' => $user['is_stuff'],
                                         'avatar' => array('id' => $user['avatar_id'], 'url' => $user['path']),
                                         'role' => $user['role_name'],
                                         'company' => array('id' => $user['company_id'], 'name' => $user['company_name'], 'location' => $user['location'])
                        );
            
                $output[] = $single_user;
            }
            
            echo json_encode($output);
    }
    
    public function getNewsByUserId($userId){
        
        $news_table = $this->join('News', 'user.user_id', '=', 'News.user_id')->orderBy('news_id', 'desc')->where('user.user_id', $userId)->get()->all();
        $output = array();
        
        foreach ($news_table as $new) {
            $single_news = array(
                                 'text' => $new['text'],
                                 'type' => $new['type'],
                                 'id' => $new['news_id']
                );
            $output[] = $single_news;
        }
        
        echo json_encode($output);
            
    }
    
    public function getCourseByUserId($userId){
        
        $course_table = $this->join('user_course', 'user.user_id', '=', 'user_course.user_id')->
                               join('course', 'user_course.course_id', '=', 'course.course_id')->
                               join('image', 'course.image_id', '=', 'image.image_id')->where('user.user_id', $userId)->get()->all();
        $output = array();
        $video  = new Video;
        $user_course = new user_course;
        
        foreach ($course_table as $course) {
            
            $single_course = array(
                                 'id' => $course['course_id'],
                                 'title' => $course['title'],
                                 'image' => $course['path'],
                                 'videos' => $video->getVideo($course)
                                 );
            
            $table_presentation = $user_course->
                            join('presentation', 'user_course.course_id', '=', 'presentation.course_id')->where('user_course.course_id', $course['course_id'])->get()->all();
                
            $presentation_full_output = array();
            $presentation_output = array();
            
            foreach ($table_presentation as $presentation) {
                    
                    $presentation_output = array(
                                                 'id'           => $presentation['presentatnion_id'],
                                                 'description'  => $presentation['description'],
                                                 'title'        => $presentation['title'],
                                                 'slides'       => array(),
                                                 'answerable'   => $presentation['answerable']
                        );
                        
                    $presentation_full_output[] = $presentation_output;
        
            }
            $single_course['presentations'] = $presentation_full_output;
            $single_course['answerable'] = "czego ma dotyczyć to pole";
            
            $output[] = $single_course;
        }
        
        echo json_encode($output);
            
    }
    
    public function getUserAnswerByUserId($userId){
        $answer_table = $this->join('user_answer', 'user.user_id', '=', 'user_answer.user_id')->
                               join('video', 'user_answer.video_id', '=', 'video.video_id')->
                               join('image', 'user_answer.image_id', '=', 'image.image_id')->where('user.user_id', $userId)->get()->all();
        
        $output = array();
        
        foreach($answer_table as $answer){
            
            $single_answer = array(
                                    'id' =>  $answer['video_id'],
                                    'title' => $answer['title'],
                                    'image_id' => $answer['image_id'],
                                    'stream_url' => $answer['path'],
                                    'duration' => $answer['duration'],
                                    'answerable' => $answer['aswerable']
                                  );
                                  
                                  
            $output[] = $single_answer;
        }
        
        echo json_encode($output);
    }
    
    
    
}
