<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Video;
use App\User_class;

class Classes extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'class';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     
     public function getCourse($courseId = null){
         if($courseId){
             $table_course = $this->where('course_id','=', $courseId)->get();
             
             //sprawdzanie czy znalazł jakis rekord
             if(!$table_course->count()){
                 return false;
             } else {
                return $table_course;
             } 
             
         } else {
             $table_course = $this->get()->all();
             return $table_course;
         }
     }
     
     
     public function getClass($classId = null){
         if($classId){
             $output = array();
             $table_class = $this->join('company', 'class.company_id', '=' , 'company.company_id')->join('image', 'company.image_id', '=', 'image.image_id')->where('class_id','=', $classId)->get()->all();
             foreach($table_class as $class){
                
                $output['id'] = $class['class_id'];  
                $output['title'] = $class['class_name'];
                $output['image'] = $class['path'];
                 
                $user_class = new User_class;
                
                $table_video = $user_class->
                        join('user_answer', 'user_class.user_id', '=', 'user_answer.user_id')->
                        join('video_lesson', 'video_lesson.video_id', '=', 'user_answer.video_id')->
                        join('video', 'video.video_id', '=', 'video_lesson.video_id')->where('user_class.class_id', $class['class_id'])->get()->all();

                $video_full_output = array();
                $video_output = array();
                
                foreach ($table_video as $video) {
                    
                    $video_output = array(
                                          'id'         => $video['video_id'],
                                          'title'      => $video['title'],
                                          'image_id'   => $video['image_id'],
                                          'stream_url' => $video['path'],
                                          'duration'   => $video['duration'],
                                          'answerable' => $video['aswerable']
                                          );
                    $video_full_output[] = $video_output;
                }
                
                $output['videos'] = $video_full_output;
                
                $table_presentation = $user_class->
                            join('user_course', 'user_class.user_id', '=', 'user_course.user_id')->
                            join('presentation', 'user_course.course_id', '=', 'presentation.course_id')->where('user_class.class_id', $class['class_id'])->get()->all();
                
                $presentation_full_output = array();
                $presentation_output = array();
                
                foreach ($table_presentation as $presentation) {
                    
                    $presentation_output = array(
                                                 'id'           => $presentation['presentatnion_id'],
                                                 'description'  => $presentation['description'],
                                                 'title'        => $presentation['title'],
                                                 'slides'       => array(),
                                                 'answerable'   => $presentation['answerable']
                        );
                        
                    $presentation_full_output[] = $presentation_output;
                }
                
                $output['presentation'] = $presentation_full_output;
                $output['answerable'] = "czego ma dotyczyć to pole";
                
                return $output;
                
             }
         } else {
             
            $table_class = $this->join('company', 'class.company_id', '=' , 'company.company_id')->join('image', 'company.image_id', '=', 'image.image_id')->get()->all();
            
            foreach($table_class as $class){
                
                $output['id'] = $class['class_id'];  
                $output['title'] = $class['class_name'];
                $output['image'] = $class['path'];
                 
                $user_class = new user_class;
                
                $table_video = $user_class->
                        join('user_answer', 'user_class.user_id', '=', 'user_answer.user_id')->
                        join('video_lesson', 'video_lesson.video_id', '=', 'user_answer.video_id')->
                        join('video', 'video.video_id', '=', 'video_lesson.video_id')->where('user_class.class_id', $class['class_id'])->get()->all();

                $video_full_output = array();
                $video_output = array();
                
                foreach ($table_video as $video) {
                    
                    $video_output = array(
                                          'id'         => $video['video_id'],
                                          'title'      => $video['title'],
                                          'image_id'   => $video['image_id'],
                                          'stream_url' => $video['path'],
                                          'duration'   => $video['duration'],
                                          'answerable' => $video['aswerable']
                                          );
                    $video_full_output[] = $video_output;
                }
                
                $output['videos'] = $video_full_output;
                
                $table_presentation = $user_class->
                            join('user_course', 'user_class.user_id', '=', 'user_course.user_id')->
                            join('presentation', 'user_course.course_id', '=', 'presentation.course_id')->where('user_class.class_id', $class['class_id'])->get()->all();
                
                $presentation_full_output = array();
                $presentation_output = array();
                
                foreach ($table_presentation as $presentation) {
                    
                    $presentation_output = array(
                                                 'id'           => $presentation['presentatnion_id'],
                                                 'description'  => $presentation['description'],
                                                 'title'        => $presentation['title'],
                                                 'slides'       => array(),
                                                 'answerable'   => $presentation['answerable']
                        );
                        
                    $presentation_full_output[] = $presentation_output;
                }
                
                $output['presentations'] = $presentation_full_output;
                $output['answerable'] = "czego ma dotyczyć to pole";
                
                return $output;
                
             }
             
         }
         
     }
     
     
}