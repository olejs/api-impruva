<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video_lesson extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'video_lesson';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['role_id', 'role_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function getVideoByCourseId($course_id){
         $video_table = $this->join('video','video.video_id','=','video_lesson.video_id')->
                                where('video_lesson.course_id', $course_id)->get();
         if(empty($video_table)){
             return array();
         } else {
             return $video_table;
         }
     }
     
}